@echo off
REM ****************************************************************************
REM Vivado (TM) v2020.1 (64-bit)
REM
REM Filename    : simulate.bat
REM Simulator   : Xilinx Vivado Simulator
REM Description : Script for simulating the design by launching the simulator
REM
REM Generated by Vivado on Mon Dec 14 09:44:30 +0900 2020
REM SW Build 2902540 on Wed May 27 19:54:49 MDT 2020
REM
REM Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
REM
REM usage: simulate.bat
REM
REM ****************************************************************************
echo "xsim testbench_behav -key {Behavioral:sim_1:Functional:testbench} -tclbatch testbench.tcl -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/testbench.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/unit.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/controller.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/intreg.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/uart_unit.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/floatreg.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/condition_code.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/funit.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/fpu.wcfg -log simulate.log"
call xsim  testbench_behav -key {Behavioral:sim_1:Functional:testbench} -tclbatch testbench.tcl -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/testbench.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/unit.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/controller.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/intreg.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/uart_unit.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/floatreg.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/condition_code.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/funit.wcfg -view C:/Users/kubo.tatsuya/Projects/cpu_lab/core/vivado_project/vivado_project.waveforms/fpu.wcfg -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
